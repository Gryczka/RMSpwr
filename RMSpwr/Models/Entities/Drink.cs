﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class Drink
    {
        [Key]
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Amount { get; set; }
        public List<Component> Components { get; set; }
        public string Description { get; set; }
        public AdditionalDrinkInformation? AdditionalInformation { get; set; }
    }

    public enum AdditionalDrinkInformation
    {
        None, WithAlcohol, Hot, Cold
    }
}
