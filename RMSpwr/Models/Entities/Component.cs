﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class Component
    {
        [Key]
        public Guid Id { get; set; }
        public String Element { get; set; }
    }
}
