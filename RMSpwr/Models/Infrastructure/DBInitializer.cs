﻿using RMSpwr.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Infrastructure
{
    public class DbInitializer
    {
        public static void Initialize(OrderContext context)
        {
            if (!context.Components.Any())
            {
                var components = new Component[]
                {
                    new Component{Id=Guid.NewGuid(),Element="Szynka parmeńska"},
                    new Component{Id=Guid.NewGuid(),Element="Boczniak królewski"},
                    new Component{Id=Guid.NewGuid(),Element="Orzechy laskowe"},
                    new Component{Id=Guid.NewGuid(),Element="Cebula kalabryjska"},
                    new Component{Id=Guid.NewGuid(),Element="Pietruszka"},
                    new Component{Id=Guid.NewGuid(),Element="Masło"},
                    new Component{Id=Guid.NewGuid(),Element="Parmigiano-Reggiano DOP"},
                    new Component{Id=Guid.NewGuid(),Element="Sos z gorgonzoli"},
                    new Component{Id=Guid.NewGuid(),Element="Sos pomidorowy"},
                    new Component{Id=Guid.NewGuid(),Element="Wołowina Fassona Piemontese"},
                    new Component{Id=Guid.NewGuid(),Element="Pecorino Romano DOP"},
                };
                foreach (var component in components)
                {
                    context.Components.Add(component);
                }
                context.SaveChanges();
            }

            if (!context.Dishes.Any())
            {
                var dishes = new Dish[]
                {
                    new Dish{Id=Guid.NewGuid(),Name="Gnocchi",Amount=27.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDishInformation.None},
                    new Dish{Id=Guid.NewGuid(),Name="Bigoli",Amount=31.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDishInformation.None},
                };
                foreach (var dish in dishes)
                {
                    context.Dishes.Add(dish);
                }
                context.SaveChanges();
            }

            if (!context.Drinks.Any())
            {
                var drinks = new Drink[]
                {
                    new Drink{Id=Guid.NewGuid(),Name="Wino Białe",Amount=12.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDrinkInformation.WithAlcohol},
                    new Drink{Id=Guid.NewGuid(),Name="Wino Czerwone",Amount=15.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDrinkInformation.WithAlcohol},
                    new Drink{Id=Guid.NewGuid(),Name="Herbata z rumem",Amount=14.0f,Components=new List<Component>(),Description="Description",AdditionalInformation=AdditionalDrinkInformation.WithAlcohol},
                };
                foreach (var drink in drinks)
                {
                    context.Drinks.Add(drink);
                }
                context.SaveChanges();
            }

            if (!context.Tables.Any())
            {
                var tables = new Table[]
                {
                    new Table{Id=Guid.NewGuid(),Number="F1",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F2",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F3",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F4",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F5",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F6",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="F7",Location=Location.FirstRoom},
                    new Table{Id=Guid.NewGuid(),Number="S1",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S2",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S3",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S4",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S5",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S6",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="S7",Location=Location.SecondRoom},
                    new Table{Id=Guid.NewGuid(),Number="T1",Location=Location.Tarrace},
                    new Table{Id=Guid.NewGuid(),Number="T2",Location=Location.Tarrace},
                    new Table{Id=Guid.NewGuid(),Number="T3",Location=Location.Tarrace},
                    new Table{Id=Guid.NewGuid(),Number="B1",Location=Location.Balcony},
                    new Table{Id=Guid.NewGuid(),Number="B2",Location=Location.Balcony},
                    new Table{Id=Guid.NewGuid(),Number="B3",Location=Location.Balcony},
                    new Table{Id=Guid.NewGuid(),Number="B4",Location=Location.Balcony},
                };
                foreach (var table in tables)
                {
                    context.Tables.Add(table);
                }
                context.SaveChanges();
            }
        }
    }
}
