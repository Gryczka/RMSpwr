﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMSpwr.Models.Infrastructure;
using Microsoft.Extensions.Logging;
using RMSpwr.Models.Entities;

namespace RMSpwr.Controllers
{
    [Produces("application/json")]
    [Route("api/Dish")]
    public class DishController : Controller
    {
        private OrderContext _context;
        private ILogger _logger;
        public DishController(ILogger<DishController> logger, OrderContext context)
        {
            _context = context;
            _logger = logger;
        }

        // GET api/Dish
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<Dish> dishes = _context.Dishes.ToList();
            var dishesObjectified = dishes.Select(q => new
            {
                Id = q.Id,
                Name = q.Name,
                Amount = q.Amount,
                Components = q.Components,
                Description = q.Description,
                AdditionalInformation = q.AdditionalInformation
            });
            return Ok(dishesObjectified);
        }

        // GET api/Dish/Compontents
        [HttpGet("Components")]
        public async Task<IActionResult> ComponentsList()
        {
            List<Component> components = _context.Components.ToList();
            var componentsObjectified = components.Select(q => new
            {
                Id = q.Id,
                Element = q.Element,
            });
            return Ok(componentsObjectified);
        }
    }
}