﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class Dish
    {
        [Key]
        public Guid Id { get; set; }
        public String Name { get; set; }
        public float Amount { get; set; }
        public List<Component> Components { get; set; }
        public String Description { get; set; }
        public AdditionalDishInformation? AdditionalInformation { get; set; }
    }

    public enum AdditionalDishInformation
    {
        None, Vegetarian, Vegan, Spicy
    }
}
