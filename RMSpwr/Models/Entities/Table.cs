﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RMSpwr.Models.Entities
{
    public class Table
    {
        [Key]
        public Guid Id { get; set; }
        public string Number { get; set; }
        public Location? Location { get; set; }
    }

    public enum Location
    {
        FirstRoom, SecondRoom, Balcony, Tarrace
    }
}
