﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RMSpwr.Models.Infrastructure;
using Microsoft.Extensions.Logging;
using RMSpwr.Models.Entities;

namespace RMSpwr.Controllers
{
    [Produces("application/json")]
    [Route("api/Table")]
    public class TableController : Controller
    {
        private OrderContext _context;
        private ILogger _logger;
        public TableController(ILogger<TableController> logger, OrderContext context)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/Table
        [HttpGet]
        public async Task<IActionResult> List()
        {
            List<Table> tables = _context.Tables.ToList();
            var tablesObjectified = tables.Select(q => new
            {
                Id = q.Id,
                Number = q.Number,
                Location = q.Location
            });
            return Ok(tablesObjectified);
        }
    }
}